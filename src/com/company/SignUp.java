package com.company;

import com.company.configration.Configuration;
import com.company.json.SignUpGetJsonData;
import com.company.pom.Home;
import com.company.pom.Registration;
import com.company.pom.SignUpSignIn;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class SignUp {

    public static void main(String[] args) throws IOException, ParseException {

        //Input data from properties file
        WebDriver driver= Configuration.getConfiguration();
        SignUpGetJsonData signUpGetJsonData=new SignUpGetJsonData();
        Home home=new Home(driver);

        //SignIn Click on Home page
        home.clickSignIn();

        //Enter email and click on SignUp

        SignUpSignIn signUpSignIn=new SignUpSignIn(driver);
        signUpSignIn.enterEmailSignUp(signUpGetJsonData.getEmail_create());
        signUpSignIn.clickSubmitSignUp();

        //Enter user detail for Registration
        Registration registration=new Registration(driver);
        registration.clickGender();
        registration.enterCustomerFirstname(signUpGetJsonData.getCustomer_firstname());
        registration.enterCustomerLastname(signUpGetJsonData.getCustomer_lastname());
        registration.enterPasswd(signUpGetJsonData.getPasswd());
        registration.enterFirstname(signUpGetJsonData.getFirstname());
        registration.enterLastname(signUpGetJsonData.getLastname());
        registration.enterCompany(signUpGetJsonData.getCompany());
        registration.enterAddress(signUpGetJsonData.getAddress1());
        registration.enterCity(signUpGetJsonData.getCity());
        registration.selectState("4");
        registration.enterPostcode(signUpGetJsonData.getPostcode());
        registration.selectCountry("United States");
        registration.enterPhone(signUpGetJsonData.getPhone_mobile());
        registration.enterAlias(signUpGetJsonData.getAlias());
        registration.clickSubmit();




    }
}
