package com.company.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class SignUpGetJsonData {

    String email_create;
    String customer_firstname;
    String customer_lastname;
    String passwd;
    String firstname;
    String lastname;
    String company;
    String address1;
    String city;
    String postcode;
    String phone_mobile;
    String alias;


    public SignUpGetJsonData() throws IOException, ParseException {

        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("src/com/company/json/DataSignUp.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignUpData");

        JSONObject userDetail = (JSONObject) array.get(0);

         email_create=(String)userDetail.get("email_create");
         customer_firstname=(String)userDetail.get("customer_firstname");
         customer_lastname=(String)userDetail.get("customer_lastname");
         passwd=(String)userDetail.get("passwd");
         firstname=(String)userDetail.get("firstname");
         lastname=(String)userDetail.get("lastname");
         company=(String)userDetail.get("company");
         address1=(String)userDetail.get("address1");
         city=(String)userDetail.get("city");
         postcode=(String)userDetail.get("postcode");
         phone_mobile=(String)userDetail.get("phone_mobile");
         alias=(String)userDetail.get("alias");
    }

    public String getEmail_create() {
        return email_create;
    }

    public String getCustomer_firstname() {
        return customer_firstname;
    }

    public String getCustomer_lastname() {
        return customer_lastname;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCompany() {
        return company;
    }

    public String getAddress1() {
        return address1;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getPhone_mobile() {
        return phone_mobile;
    }

    public String getAlias() {
        return alias;
    }
}
