package com.company.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class SignInGetJsonData {

    String email;
    String password;

    public SignInGetJsonData() throws IOException, ParseException {

        JSONParser p= new JSONParser();
        Object obj=p.parse(new FileReader("src/com/company/json/DataSignUp.json"));
        JSONObject jObj =  (JSONObject)obj;
        JSONArray array=(JSONArray)jObj.get("SignInData");

        JSONObject userDetail = (JSONObject) array.get(0);

        email=(String)userDetail.get("email");
        password=(String)userDetail.get("passwd");
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
