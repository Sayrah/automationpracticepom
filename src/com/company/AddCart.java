package com.company;

import com.company.configration.Configuration;
import com.company.json.SignInGetJsonData;
import com.company.pom.*;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class AddCart {
    public static WebDriver driver;

    public static void main(String[] args) throws IOException, ParseException {


        WebDriver driver= Configuration.getConfiguration();
        SignInGetJsonData signInGetJsonData=new SignInGetJsonData();

        Home home=new Home(driver);

        //SignIn Click on Home page
        home.clickSignIn();

        //Enter email, password and click on SignUp

        SignUpSignIn signUpSignIn=new SignUpSignIn(driver);
        signUpSignIn.enterEmailSignIn(signInGetJsonData.getEmail());
        signUpSignIn.enterPassword(signInGetJsonData.getPassword());
        signUpSignIn.clickSubmitSignIn();

        //


        Dashboard dashboard=new Dashboard(driver);
        dashboard.clickWomen();
        dashboard.imageDetail();

        //

        Cart cart=new Cart(driver);
        cart.enterQuantity("2");
        cart.selectSize("M");
        cart.clickColour();
        cart.clickAddCart();

    }
}
