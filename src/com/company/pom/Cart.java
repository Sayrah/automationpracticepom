package com.company.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Cart extends PageInitialisation {

    public Cart(WebDriver driver){
        super(driver);
    }

    @FindBy(id = "quantity_wanted")
    WebElement quantity;

    @FindBy(id = "group_1")
    WebElement size;

    @FindBy(xpath = "//*[@id=\"color_8\"]")
    WebElement colour;

    @FindBy(xpath = "//p[@id='add_to_cart']/button")
    WebElement addCart;

    public void enterQuantity(String qty){
        quantity.clear();
        quantity.sendKeys(qty);
    }

    public void selectSize(String siz){
        Select select=new Select(size);
        select.selectByVisibleText(siz);
    }

    public void clickColour(){
        colour.click();
    }
    public void clickAddCart(){
        addCart.click();
    }


}
