package com.company.pom;

import com.company.pom.PageInitialisation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpSignIn extends PageInitialisation {

    public SignUpSignIn(WebDriver driver){
        super(driver);
    }
    //SignUp
    @FindBy(id = "email_create")
    WebElement emailSignUp;
    @FindBy(id = "SubmitCreate")
    WebElement submitSignUp;

    //SignIn
    @FindBy(id = "email")
    WebElement emailSignIn;
    @FindBy(id = "passwd")
    WebElement password;
    @FindBy(id = "SubmitLogin")
    WebElement submitSignIn;

    //SignUp

    public void enterEmailSignUp(String email){
        emailSignUp.sendKeys(email);
    }

    public void clickSubmitSignUp() {
        submitSignUp.click();
    }

    //SignIn
    public void enterEmailSignIn(String email){
        emailSignIn.sendKeys(email);
    }
    public void enterPassword(String pass){
        password.sendKeys(pass);
    }
    public void clickSubmitSignIn() {
        submitSignIn.click();
    }

}
