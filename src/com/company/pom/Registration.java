package com.company.pom;

import com.company.pom.PageInitialisation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Registration extends PageInitialisation {

    public Registration(WebDriver driver){
        super(driver);
    }

    @FindBy(id = "id_gender1")
    WebElement genderMale;

    @FindBy(id = "customer_firstname")
    WebElement customerFirstname;

    @FindBy(id = "customer_lastname")
    WebElement customerLastname;

    @FindBy(id = "passwd")
    WebElement passwd;

    @FindBy(id = "firstname")
    WebElement firstname;

    @FindBy(id = "lastname")
    WebElement lastname;

    @FindBy(id = "company")
    WebElement company;

    @FindBy(id = "address1")
    WebElement address;

    @FindBy(id = "city")
    WebElement city;

    @FindBy(id = "id_state")
    WebElement state;

    @FindBy(id = "postcode")
    WebElement postcode;

    @FindBy(id = "id_country")
    WebElement country;

    @FindBy(id = "phone_mobile")
    WebElement phone;

    @FindBy(id = "alias")
    WebElement alias;

    @FindBy(id = "submitAccount")
    WebElement registration;


    public void clickGender(){
        genderMale.click();
    }
    public void enterCustomerFirstname(String name){
        customerFirstname.sendKeys(name);
    }
    public void enterCustomerLastname(String name){
        customerLastname.sendKeys(name);
    }
    public void enterPasswd(String password){
        passwd.sendKeys(password);
    }

    public void enterFirstname(String name){
        firstname.sendKeys(name);
    }
    public void enterLastname(String name){
        lastname.sendKeys(name);
    }
    public void enterCompany(String com){
        company.sendKeys(com);
    }
    public void enterAddress(String address1){
        address.sendKeys(address1);
    }
    public void enterCity(String city1){
        city.sendKeys(city1);
    }
    public void selectState(String value){
        Select select=new Select(state);
        select.selectByValue(value);
    }
    public void enterPostcode(String pin){
        postcode.sendKeys(pin);
    }
    public void selectCountry(String country1){
        Select select=new Select(country);
        select.selectByVisibleText(country1);
    }
    public void enterPhone(String phone1){
        phone.sendKeys(phone1);
    }
    public void enterAlias(String alias1){

        alias.clear();
        alias.sendKeys(alias1);
    }
    public void clickSubmit(){
        registration.click();
    }



}
