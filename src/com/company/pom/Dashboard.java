package com.company.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class Dashboard extends PageInitialisation {
    public WebDriver driver;
    public Dashboard(WebDriver driver){
        super(driver);
        this.driver=driver;

    }
    //SignUp

    //SignIn

    @FindBy(linkText = "WOMEN")
    WebElement women;

    @FindBy(xpath = "//*[@id='center_column']/ul/li[2]/div/div[1]/div/a[1]/img")
    WebElement img;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/div[2]/a[2]")
    WebElement imgMore;

    Actions actions;


    public void clickWomen(){
        women.click();
    }
    public void imageDetail(){
        Actions actions=new Actions(driver);
        actions.moveToElement(img).moveToElement(imgMore).click().perform();

    }






}
