package com.company.pom;

import com.company.pom.PageInitialisation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home extends PageInitialisation {

    public Home(WebDriver driver){
        super(driver);
    }
    @FindBy(linkText = "Sign in")
    WebElement signIn;

    public void clickSignIn(){
        signIn.click();
    }


}
